using System.Collections.Generic;
using System.Reflection;
using Microsoft.AspNetCore.Mvc;

namespace LeanChat.Controllers
{
    [Route("version")]
    [ApiController]
    public class VersionController : ControllerBase
    {
        [HttpGet("")]
        public ActionResult<string> Get()
        {
            var attrib = typeof(VersionController).Assembly
                .GetCustomAttribute<AssemblyInformationalVersionAttribute>();
            return attrib.InformationalVersion;
        }
    }
}
