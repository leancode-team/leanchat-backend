using System;
using LeanChat.Model;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Serilog;
using Serilog.Events;

namespace LeanChat
{
    public class Program
    {
        public static void Main(string[] args)
        {
            if (args.Length == 1 && args[0] == "migrate")
            {
                Migrate<LeanChatDbContext>(args);
            }
            else
            {
                CreateWebHostBuilder(args).Build().Run();
            }
        }

        public static void Migrate<TContext>(string[] args, params object[] additionalArgs)
            where TContext : DbContext
        {
            var contextName = typeof(TContext).Name;

            Console.WriteLine($"Starting migration of {contextName}");

            using (var ctx = new DbContextFactory<TContext>(additionalArgs).CreateDbContext(args))
            {
                ctx.Database.Migrate();
            }

            Console.WriteLine($"Migration of {contextName} completed");
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args)
        {
            return WebHost.CreateDefaultBuilder(args)
                .ConfigureLogging(ConfigureLogger)
                .UseStartup<Startup>();
        }

        private static void ConfigureLogger(WebHostBuilderContext ctx, ILoggingBuilder builder)
        {
            var cfg = new LoggerConfiguration()
                .Enrich.FromLogContext()
                .MinimumLevel.Is(LogEventLevel.Debug)
                .MinimumLevel.Override("Microsoft", LogEventLevel.Warning)
                .MinimumLevel.Override("System", LogEventLevel.Warning);
            if (ctx.HostingEnvironment.IsDevelopment())
            {
                cfg = cfg.WriteTo.Console();
            }
            else
            {
                cfg = cfg
                    .WriteTo.Console(new Serilog.Formatting.Compact.RenderedCompactJsonFormatter());
            }
            Log.Logger = cfg.CreateLogger();

            builder.AddSerilog();
        }
    }

}
