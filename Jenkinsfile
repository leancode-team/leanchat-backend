def currentVersion = "0.1";

leancode.builder('leanchat-backend')
    .disableSlack()
    .withCustomJnlp()
    .withDocker()
    .withDotnet()
    .withDeployTools()
    .run {

    def scmVars

    stage('Checkout') {
        scmVars = safeCheckout scm
    }

    leancode.configureRepositories()

    stage('Version') {
        env.GIT_COMMIT = scmVars.GIT_COMMIT
        env.AUTHOR = BRANCH_NAME.replaceAll("/", "").replaceAll("-", "");
        env.APP_VERSION = "${currentVersion}.${nextBuildNumber()}"
        echo "Building version: ${env.AUTHOR} ${env.APP_VERSION}"

        currentBuild.displayName = "${env.AUTHOR}-${env.APP_VERSION}"
    }

    container('dotnet') {
        stage('Build') {
            sh 'dotnet publish -c Release -o deploy'
        }
    }

    stage('Push containers') {
        leancode.withACR {
            def web = docker.build(
                "${leancode.ACR()}/leanchat-backend-${env.AUTHOR}:$APP_VERSION",
                '-f release/Dockerfile .')
            web.push()
        }
    }

    stage('Deploy to test') {
        container('deploy-tools') {
            sh 'envsubst < release/deploy.yaml > deploy.yaml'
            sh 'kubectl apply -f deploy.yaml'

            archiveArtifacts artifacts: '*.yaml', fingerprint: true
        }
    }
}