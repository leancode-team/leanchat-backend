#!/usr/bin/env zsh

server=$1
uname=$2
password=$3

database=$4
new_password=$5

sqlcmd -S $server -U $uname -P $password -d $database -Q "CREATE USER app WITH PASSWORD='$new_password'; EXEC sp_addrolemember 'db_owner', 'app';"
