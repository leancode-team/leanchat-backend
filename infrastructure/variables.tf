variable "fb_secret" {
  type = string
}

variable "firebase_secret" {
  type = string
}

variable "giphy_api_key" {
  type = string
}

variable "all_call_to_preview" {
  default = false
  type    = bool
}
