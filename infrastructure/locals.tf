locals {
  envs = ["preview"]

  tags = {
    type        = "project"
    project     = "leanchat"
    environment = "playground"
  }
}
