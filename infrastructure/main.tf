provider "azurerm" {
  version         = "~> 1.29.0"
  subscription_id = "f1922be4-e112-4538-afc7-939a22a449ed"
}

provider "azurerm" {
  version         = "~> 1.29.0"
  alias           = "internal"
  subscription_id = "c4c339a4-712f-48e2-a09a-bcea4a9c9ae6"
}

provider "kubernetes" {
  version = "~> 1.7.0"
}

provider "null" {
  version = "~> 2.1"
}

provider "random" {
  version = "~> 2.1"
}

data "azurerm_resource_group" "k8s" {
  provider = azurerm.internal
  name     = "leancode-internal-k8s"
}

data "azurerm_virtual_network" "k8s" {
  provider            = azurerm.internal
  name                = "leancode-internal-k8s"
  resource_group_name = "leancode-internal-k8s"
}

data "azurerm_subnet" "k8s" {
  provider             = azurerm.internal
  name                 = "k8s"
  resource_group_name  = "leancode-internal-k8s"
  virtual_network_name = "leancode-internal-k8s"
}

resource "azurerm_resource_group" "main" {
  name     = "leanchat"
  location = "West Europe"

  tags = local.tags
}

resource "random_string" "db_sa" {
  length  = 32
  special = false

  keepers = {
    rg_id = azurerm_resource_group.main.id
  }
}

resource "azurerm_sql_server" "main" {
  name                = "leanchat"
  resource_group_name = "${azurerm_resource_group.main.name}"
  location            = "${azurerm_resource_group.main.location}"

  version                      = "12.0"
  administrator_login          = "leanchat_sa"
  administrator_login_password = "${random_string.db_sa.result}"

  tags = local.tags
}

resource "azurerm_sql_firewall_rule" "allow-all" {
  name                = "allow-all"
  resource_group_name = azurerm_resource_group.main.name
  server_name         = azurerm_sql_server.main.name

  start_ip_address = "0.0.0.0"
  end_ip_address   = "255.255.255.255"
}

resource "azurerm_sql_virtual_network_rule" "allow-k8s-endpoints" {
  name                = "allow-k8s-endpoints"
  resource_group_name = azurerm_resource_group.main.name
  server_name         = azurerm_sql_server.main.name
  subnet_id           = data.azurerm_subnet.k8s.id
}

resource "random_string" "db_app" {
  count   = length(local.envs)
  length  = 32
  special = false

  keepers = {
    rg_id = local.envs[count.index]
  }
}

resource "azurerm_sql_database" "dbs" {
  count = length(local.envs)

  resource_group_name = azurerm_resource_group.main.name
  location            = azurerm_resource_group.main.location
  server_name         = azurerm_sql_server.main.name

  name = local.envs[count.index]

  edition   = "Basic"
  collation = "Polish_CS_AI"

  provisioner "local-exec" {
    command = "./create_user.sh ${azurerm_sql_server.main.fully_qualified_domain_name} leanchat_sa ${random_string.db_sa.result} ${local.envs[count.index]} ${random_string.db_app[count.index].result}"
  }
}

resource "null_resource" "secrets" {
  count = length(local.envs)

  triggers = {
    name     = local.envs[var.all_call_to_preview ? 0 : count.index]
    server   = azurerm_sql_server.main.fully_qualified_domain_name
    password = random_string.db_app[var.all_call_to_preview ? 0 : count.index].result
  }
}

resource "kubernetes_secret" "app" {
  count = length(local.envs)

  metadata {
    name      = "app-secrets-${null_resource.secrets[count.index].triggers.name}"
    namespace = "leanchat"

    labels = {
      type      = "app"
      app       = "leanchat"
      component = "config"
      author    = null_resource.secrets[count.index].triggers.name
    }
  }

  data = {
    ConnectionStrings__Database = "Server=tcp:${null_resource.secrets[count.index].triggers.server},1433;Initial Catalog=${null_resource.secrets[count.index].triggers.name};Persist Security Info=False;User ID=app;Password=${null_resource.secrets[count.index].triggers.password};MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;"
    Secrets__FacebookAppSecret  = var.fb_secret
    Secrets__Firebase           = var.firebase_secret
    Secrets__Giphy              = var.giphy_api_key
    Author                      = null_resource.secrets[count.index].triggers.name
  }
}
