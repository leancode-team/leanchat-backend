using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace LeanChat.Model
{
    sealed class DbContextFactory<TContext> : IDesignTimeDbContextFactory<TContext>
        where TContext : DbContext
    {
        private const string ConnectionStringKey = "ConnectionStrings:Database";
        private static string ConnectionStringDenormalizedKey => ConnectionStringKey.Replace(":", "__");

        private readonly object[] additionalArgs;

        public DbContextFactory(object[] additionalArgs)
        {
            this.additionalArgs = additionalArgs;
        }

        public TContext CreateDbContext(string[] args)
        {
            var services = new ServiceCollection();
            services.AddLogging(cfg => cfg.AddConsole());
            var provider = services.BuildServiceProvider();

            var opts = new DbContextOptionsBuilder<TContext>()
                .UseLoggerFactory(provider.GetRequiredService<ILoggerFactory>())
                .UseSqlServer(GetConnectionString())
                .Options;

            return (TContext)Activator.CreateInstance(
                typeof(TContext),
                new object[] { opts }.Concat(additionalArgs ?? Array.Empty<object>()).ToArray());
        }

        public static string GetConnectionString()
        {
            var connectionString = Environment.GetEnvironmentVariable(ConnectionStringKey);

            if (string.IsNullOrEmpty(connectionString))
            {
                return Environment.GetEnvironmentVariable(ConnectionStringDenormalizedKey);
            }
            else
            {
                return connectionString;
            }
        }
    }
}